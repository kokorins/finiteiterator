#include "finiterange.h"
#include <algorithm>
#include <functional>
#include <numeric>

FiniteIterator& FiniteIterator::operator++() {
  _range.increment(*this);
  return *this;
}

const std::vector<size_t>& FiniteIterator::operator*()const {
  return state();
}

bool FiniteIterator::operator !=(const FiniteIterator& rhs) const{
  return &_range!=&rhs._range || _state != rhs._state;
}

const std::vector<size_t>& FiniteIterator::state()const {
  return _state;
}

FiniteRange::FiniteRange(const std::vector<size_t>& max_values):
  _max_values(max_values)
{
}

size_t FiniteRange::size()const {
  return std::accumulate(_max_values.begin(), _max_values.end(), 
                         (size_t)1, std::multiplies<size_t>());
}

FiniteRange::iterator FiniteRange::begin()const {
  return FiniteIterator(std::vector<size_t>(_max_values.size()), *this);
}

FiniteRange::iterator FiniteRange::end() const {
  return FiniteIterator(_max_values, *this);;
}

void FiniteRange::increment(FiniteIterator& iter) const {
  size_t idx = 0;
  while(idx<_max_values.size() && ++iter._state[idx]==_max_values[idx])
    ++idx;
  if(idx < _max_values.size())
    std::fill(iter._state.begin(), iter._state.begin()+idx, 0);
}
