#ifndef FINITERANGE_H_
#define FINITERANGE_H_

#include <vector>
#include <cstddef>

class FiniteRange;

/**
 * \brief Implements iterator over FiniteRange class elements.
 * \sa FiniteRange
 */
class FiniteIterator {
  friend class FiniteRange;
private:
  FiniteIterator(const std::vector<size_t>& state, const FiniteRange& range):
                 _state(state), _range(range){}
public:
  /**
   * Iterate to the next element
   * @return Next element
   */
  FiniteIterator& operator++();
  /**
   * Compares two iterator
   * @param rhs right hand side iterator
   * @return if their states or ancestors differs.
   */
  bool operator!=(const FiniteIterator& rhs)const;
  /**
   * Get the current state
   * @return current state
   * \sa state()
   */
  const std::vector<size_t>& operator*()const;
  /**
   * current iterator state
   * @return indices of current state
   */
  const std::vector<size_t>& state()const;
private:
  std::vector<size_t> _state;
  const FiniteRange& _range;
};

/**
 * \brief Realization of the elements iterator over the finite multi-index of elements.
 *  Imagine that there is a vector of n elements (n1,n2,...).
 *  We need to iterate over all combinations, for example n=2, n1=3, n2=2
 *  so all possibilities are: (0,0), (0,1), (1,0), (1,1), (2,0), (2,1)
 *  Elements are iterates in a lexicographic order.
 */
class FiniteRange {
public:
  typedef FiniteIterator iterator;
public:
  /**
   * @param max_values vector of (n1, n2,...)
   */
  explicit FiniteRange(const std::vector<size_t>& max_values);
  /**
   * @return Number of combinations
   */
  size_t size()const;
  /**
   * @return First combination pointer
   */
  iterator begin()const;
  /**
   * @return After last combination pointer
   */
  iterator end()const;
  /**
   * Move iterator one combination forward
   * @param iter Iterator to be modified
   */
  void increment(FiniteIterator& iter)const;
private:
  std::vector<size_t> _max_values;
};

#endif /* FINITERANGE_H_ */
