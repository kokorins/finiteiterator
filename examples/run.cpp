#include <finiteiterator/finiterange.h>
#include <vector>
#include <cstddef>
#include <iostream>

int main() {
  std::vector<size_t> vec(5,4);
  vec[0] = 1;
  vec[1] = 6;
  vec[2] = 1;
  vec[3] = 1;
  vec[4] = 6;
  FiniteRange fin(vec);
  for(FiniteRange::iterator it = fin.begin(); it!=fin.end(); ++it) {
    for(size_t i=0; i<(*it).size(); ++i)
      std::cout<<(*it)[i]<<" ";
    std::cout<<std::endl;
  }
  return 0;
}
